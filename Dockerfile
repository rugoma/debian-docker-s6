## Adaptaciones desde el contenedor Debian-S6-overlay de Oznu
## Y desde los contenedores de Linuxserver.io
FROM debian:stretch-slim 

# set version label
ARG BUILD_DATE
ARG VERSION
LABEL build_version="rgomeza-Friomago version:- ${VERSION} Build-date:- ${BUILD_DATE}"
LABEL maintainer="rgomeza"

# Variables de entorno
ARG DEBIAN_FRONTEND="noninteractive"

## Variable S6
ENV ARQ_S6=amd64
ENV VER_S6=v1.22.1.0

ENV LOCALES=es_ES.UTF-8
                                                                                                   
RUN apt-get update \                                                                               
  && apt-get install -y apt-utils locales \                                                        
  && apt-get install -y curl tzdata \                                                              
  && locale-gen ${LOCALES} \                                                                      
  && curl -SLO "https://github.com/just-containers/s6-overlay/releases/download/${VER_S6}/s6-overlay-${ARQ_S6}.tar.gz" \
  && tar -xzf s6-overlay-${ARQ_S6}.tar.gz -C / \
  && tar -xzf s6-overlay-${ARQ_S6}.tar.gz -C /usr ./bin \
  && rm -rf s6-overlay-${ARQ_S6}.tar.gz \
  && useradd -u 911 -U -d /config -s /bin/false abc \                                              
  && usermod -G users abc \                                                                        
  && mkdir -p /app /config /defaults \
  && apt-get clean \
  && rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*

COPY rootfs /

ENTRYPOINT [ "/init" ]

